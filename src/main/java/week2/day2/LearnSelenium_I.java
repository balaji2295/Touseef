package week2.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnSelenium_I {

	public static void main(String[] args) {
		
		// Setting environment to run the selenium code on the browser
		//System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		
		// To Open a Chrome Browser
		ChromeDriver driver = new ChromeDriver();
		
		// Load an url
		driver.get("https://login.salesforce.com/");
		
		// To Maximise the browser
		driver.manage().window().maximize();
		
		// To find an element
		WebElement eleUsername = driver.findElement(By.id("username"));
		// To type on an element
		eleUsername.sendKeys("cypress@testleaf.com");
		
		driver.findElement(By.id("password")).sendKeys("Bootcamp$123",Keys.ENTER);
		
		
		
		
		
	}

}
