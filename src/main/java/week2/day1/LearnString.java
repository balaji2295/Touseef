package week2.day1;

public class LearnString {
public static void main(String[] args) {
	// String Literal
	String text1 = "Touseef";
	// String Object
	String text2 = new String("Touseef"); 
	
	String text3 = "Touseef";
	if(text1==text3) {
		System.out.println("Text matches");
	}
	else {
		System.out.println("Text mismatch");
	}
	
}
}
