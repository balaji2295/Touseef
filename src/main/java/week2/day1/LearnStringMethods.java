package week2.day1;

public class LearnStringMethods {
public static void main(String[] args) {
	String text = "Touseef";
	// To Upper case
	String upperCase = text.toUpperCase();
	// To Lower case
	String lowerCase = text.toLowerCase();
	// To remove unwanted spaces in the front & back of the String
	String text1 = "    I live in NW    ";
	String trim = text1.trim();
	// String Comparison
	// Equals
	String text2 = "Touseef";
	String text3 = "touseef";
	boolean equals = text2.equals(text3);
	// Equal ignore the case
	boolean equalsIgnoreCase = text2.equalsIgnoreCase(text3);
	// Partly Verifying
	String text4 = "I live in Texas";
	String text5 = "in texas";
	boolean contains = text4.contains(text5);
	// Number of chars
	String text6 = "Chennai";
	int length = text6.length();
	// Pulls an individual char in the given index
	for(int i = 0; i < text6.length(); i++) {
	char charAt = text6.charAt(i);
	//System.out.println(charAt);
	}
	
	// Take the chars from the String
	char[] charArray = text6.toCharArray();
	for (int i = 0; i < charArray.length; i++) {
		//System.out.println(charArray[i]);
	}
	String str = "I belong to Nagarkovil";
	// Pull words from my String
	String[] split = str.split(" ");
	for (int i = 0; i < split.length; i++) {
	//	System.out.println(split[i]);
	}
	// To concatinate two Strings
	String concat = str.concat(", South Tamil Nadu");
	//System.out.println(str + ", South Tamil Nadu");
	
	boolean startsWith = str.startsWith("I");
	boolean endsWith = str.endsWith("Nagarkovil");
	String str1 = "Touseef Ahamed";
	String substring = str1.substring(8);
	//System.out.println(substring);
	
	String substring2 = str1.substring(0, 7);
	//System.out.println(substring2);
	
	
	String replace = str1.replace('e', '$');
	System.out.println(replace);
	
	String txt = "Estb 1995";
	String replaceAll = txt.replaceAll("[^0-9]", "");
	System.out.println(replaceAll);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
}
