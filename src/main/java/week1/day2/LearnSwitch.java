package week1.day2;

public class LearnSwitch {
public static void main(String[] args) {
	String seat = "Gold";
	switch (seat) {
	case "First Class":
		System.out.println("The price is Rs.120");
		break;
	case "Second Class":
		System.out.println("The price is Rs.100");
		break;
	case "Third Class":
		System.out.println("The price is Rs.80");
		break;
	case "Balcony":
		System.out.println("The price is Rs.150");
		break;
	default:
		System.out.println("Please Select a valid category");
		break;
	}
}
}
