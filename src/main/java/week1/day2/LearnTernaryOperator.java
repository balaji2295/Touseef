package week1.day2;

public class LearnTernaryOperator {
public static void main(String[] args) {
	int a = 123, b = 27, c =244;
	
	System.out.println(a>b&&a>c ? "A is greater": b>a&&b>c ? "B is greater":"C is greater");
	// Condition ? True Block : False Block
	//System.out.println(a>b ? "A is greater" : "B is greater");
	
	/*if(a>b) {
		System.out.println("A is greater");
	}
	else {
		System.out.println("B is greater");
	}*/
}
}