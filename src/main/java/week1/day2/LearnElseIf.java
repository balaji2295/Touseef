package week1.day2;

public class LearnElseIf {
public static void main(String[] args) {
	int mark = 35;
	if(mark >= 80 && mark <= 100) {
		System.out.println("A Grade");
	}
	else if(mark >= 60 && mark < 80) {
		System.out.println("B Grade");
	}
	else if(mark >= 40 && mark < 60) {
		System.out.println("C Grade");
	}
	else {
		System.out.println("Re-Appear");
	}
}
}
