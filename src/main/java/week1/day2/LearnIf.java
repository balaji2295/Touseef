package week1.day2;

public class LearnIf {
public static void main(String[] args) {
	String title = "Welcome to CRM";
	
	// Checking the condition using if
	if(title.equals("Welcome to crm")) {
		
		// True block
		System.out.println("Title Verified");
		
	}
	else {
		
		//false block
		System.out.println("Title mismatch");
	}
	
	System.out.println("Out of condition");
	
}
}
