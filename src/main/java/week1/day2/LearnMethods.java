package week1.day2;

public class LearnMethods {
	int a = 10;
	// Access_Modifer return_type methodName(inputs){    }

		// No input/ No output method
		public void displayName() {
			System.out.println("Welcome Touseef!");
		}
		
		// No output / Given Input
		public void displayGivenName(String name) {
			System.out.println("Welcome to "+name);
		}
		
		// Return output / No input
		public long getPhoneNumber() {
			long phoneNumber = 98765432102L;
			return phoneNumber;
		}
		
		public int getBanana(int paise) {
			return paise*2;
		}
		
		public static void main(String[] args) {
			// Calling a Method
			
			// Step 1: Create an Object
			LearnMethods object = new LearnMethods();
			
			// Step 2: Call the method using the object
			object.displayName();
			
			object.displayGivenName("Selenium");
			
			long pn = object.getPhoneNumber();
//			System.out.println(object.getPhoneNumber());
			
			int count = object.getBanana(5);
			System.out.println(count);
			
			
		}
}
