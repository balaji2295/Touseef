package week1.day2;

public class LearnForLoop {
public static void main(String[] args) {
	// for(initialization ; condition ; Increment/decrement){   }
	
	for (int i = 1 ; i <= 10 ; i++) {
		System.out.println(i);
	}
	
	for (int i = 10 ; i >= 1 ; i--) {
		System.out.println(i);
	}
	
	for (int i = 1 ; i <= 10 ; i++) {
		System.out.println("Touseef");
	}
}
}
