package week1.day1;

public class LearnAccessModifiers {
// Access Modifiers
	// 1. public
	// 2. private
	// 3. package/default/no-modifier
	// 4. protected
	
// Class -> public & package
// variables & methods -> public,private,default & protected
	public int num1 = 5;
	private int num2 = 10;
	int num3 = 15;
	
	public void viewVisibilty() {
		// Classname variable_name = new Constructor;
		LearnAccessModifiers obj = new LearnAccessModifiers();
		int num12 = obj.num1;
		int num22 = obj.num2;
		int num32 = obj.num3;
	}
}
