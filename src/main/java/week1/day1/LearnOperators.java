package week1.day1;

public class LearnOperators {
// Operators -> unary,binary & tertiary
	
	public static void main(String[] args) {
		
		// Unary Operator
		int x = 10;
		System.out.println(++x);
		x++; // x=x+1;
		System.out.println(x);
		
		//Binary operator
		int a=5,b=7, c=0;
		c=a+b;
		// Arithmetic operator -> +,-,*,/,%
		// / -> 4/2=2
		// % -> 4%2=0
		
		// Relational Operator -> <,>,<=,>=,==,!=
		
		// Assignment Operator -> int a = 10;
		
		// Logical Operator -> AND -> && OR -> || NOT -> !
	}
}
