package week1.day1;

public class LearnDatatype {
	// access_modifier datatype variable_name = value;
int num1 = 1000011122;
short num2 = 100;
long num3 = 98765432100123L;
float num4 = 123.004F;
double num5 = 2225336.21458;
char num6 = 'a';  // 'a','A','1','$','\n'
boolean num7 = true;	// true false
String name = "Touseef";

public void viewVisibilty() {
	
	LearnAccessModifiers obj = new LearnAccessModifiers();
	System.out.println(obj.num1);
	//int num22 = obj.num2;
	int num32 = obj.num3;
}

}
