package week3.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnVerificationMethods {
public static void main(String[] args) {
	ChromeDriver driver = new ChromeDriver();
	WebElement element = driver.findElement(By.id("username"));
	// clear() -> textbox -> to clear the default value
	element.clear();
	// click() -> buttons, links, radio buttons, check box -> click on the element
	element.click();
	// findElement() -> helps us to find and interact with the nested element or sub element
	element.findElement(By.id(""));
	// findElements() -> helps us to find and interact with the nested elements or sub elements
	element.findElements(By.id(""));
	// getAttribute() -> any attribute value of the element
	String attributevalue = element.getAttribute("id");
	// getCssValue() -> any css property of the element
	String cssValue = element.getCssValue("background-color");
	// getLocation() -> retrieves element x & y coordinates point 
	Point location = element.getLocation();
	int x = location.getX();
	int y = location.getY();
	// getSize() -> height and width of the element
	Dimension size = element.getSize();
	int height = size.getHeight();
	int width = size.getWidth();
	// getTagName() -> tag name of the found element
	String tagName = element.getTagName();
	// getText() -> retrieve the text from the page
	String text = element.getText();
	// isDisplayed() -> verify the element is visible or not
	boolean displayed = element.isDisplayed();
	// enabled() -> verify the element is interactable
	boolean enabled = element.isEnabled();
	// isSelected() -> verify the button / radio button / check box is already selected 
	boolean selected = element.isSelected();
	// sendKeys() -> to type and to send keyboard keys to the web page
	element.sendKeys("Touseef",Keys.ARROW_DOWN,Keys.ENTER);
	// submit() -> form
	element.submit();
	
	
	
	
	
	
	
	
}
}
