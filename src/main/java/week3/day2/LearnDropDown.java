package week3.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnDropDown {
public static void main(String[] args) {

	// Setting environment to run the selenium code on the browser
	//System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	WebDriverManager.chromedriver().setup();
	
	// To Open a Chrome Browser
	ChromeDriver driver = new ChromeDriver();
	
	// To load the URL
	driver.get("http://leaftaps.com/opentaps/");
	
	// To Maximise the browser
	driver.manage().window().maximize();
	
	// To Set implicit Wait
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	// To Enter Username
	driver.findElement(By.id("username")).sendKeys("Demosalesmanager");
	
	// To Enter Password
	driver.findElement(By.id("password")).sendKeys("crmsfa",Keys.ENTER);
	
	// Click CRMSFA Link
	driver.findElement(By.linkText("CRM/SFA")).click();
	
	// Click on Leads
	driver.findElement(By.linkText("Leads")).click();
	
	// Click on Create Lead
	driver.findElement(By.linkText("Create Lead")).click();
	
	// To interact with drop down
	
	WebElement elementDropDown = driver.findElement(By.id("createLeadForm_dataSourceId"));
	// Solution 1: using sendKeys
	// elementDropDown.sendKeys("Other");
	
	// Solution 2: using Select Class
	Select dropDown = new Select(elementDropDown);
	// Solution 2.1: using Select class & index (Index Starts with 0)
	// dropDown.selectByIndex(2);
	
	// Solution 2.2: using Select class & Visible Text in the Drop down
	// dropDown.selectByVisibleText("Website");
	
	// Solution 2.3: using Select class & value attribute in the options tag
	// dropDown.selectByValue("LEAD_PR");
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
}
